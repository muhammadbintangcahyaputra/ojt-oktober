<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

	public function index()
	{
		$data['barang'] = $this->db->get('barang')->result_array();
		$this->load->view('barang/index', $data);
	}

	public function tambah()
	{
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('harga', 'harga', 'required|numeric');

		if ($this->form_validation->run() == False) {
			$this->load->view('barang/tambah');
		} else {
			$data = [
				'nama' => $this->input->post('nama'),
				'harga' => $this->input->post('harga')
			];
			$this->db->insert('barang', $data);
			$this->session->set_flashdata('msg', 'Dimasukan');
			redirect(base_url('index.php/barang/index'));
		}
	}
	public function update($id)
	{
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('harga', 'harga', 'required|numeric');

		if ($this->form_validation->run() == False) {
			$data['barang'] = $this->db->get_where('barang', ['id' => $id])->row_array();
			$this->load->view('barang/update', $data);
		} else {
			$data = [
				'nama' => $this->input->post('nama'),
				'harga' => $this->input->post('harga')
			];
			$this->db->update('barang', $data, ['id' => $id]);
			$this->session->set_flashdata('msg', 'Diubah');
			redirect(base_url('index.php/barang/index'));
		}
	}

	public function delete($id)
	{
		$this->db->delete('barang',['id' => $id]);
		$this->session->set_flashdata('msg', 'Dihapus');
		redirect(base_url('index.php/barang/index'));
	}
}
