# ambil library
from flask import Flask, jsonify, request
import jwt
import datetime
from functools import wraps


# inisiasi aplikasi flask
app = Flask(__name__)
app.config["SECRET_KEY"] = "@(*)#$iowpmcieupONPOI09834"

# validasi keamanan
def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        token = request.headers.get("JWT")
        if not token:
            return jsonify({"data":"Token Required", "code": 403}), 403        
        try:
            data = jwt.decode(token, app.config["SECRET_KEY"])
            print(data)
            # return data
        except:
            return jsonify({"data":"Token Invalid", "code": 403}), 403
        return f(*args, **kwargs)
    return decorator


# buat rute backend
@app.route("/")
def root():
	data = [
		{
			"nama" : "Buku",
			"harga" : 5000
		},
		{
			"nama" : "Tas",
			"harga" : 50000
		},
		{
			"nama" : "Pensil",
			"harga" : 4000
		},
	]
	return jsonify(data)

@app.route("/karyawan")
@token_required
def karyawan():
	data = [
		{
			"nama" : "Budi",
			"kelas" : "Datascience"
		},
		{
			"nama" : "Bintang",
			"kelas" : "Cyber"
		},
		{
			"nama" : "Bumi",
			"kelas" : "Fullstack"
		},
	]
	return jsonify(data)

@app.route("/login", methods=["POST"])
def login():
    try:
        data = request.json
        if (data['username'] == "admin" and data['password'] == "admin123"):
            token = jwt.encode({"data": data['username']+"-"+request.remote_addr, "exp": datetime.datetime.utcnow()+datetime.timedelta(minutes=30)}, app.config["SECRET_KEY"])
            return jsonify({"data": token.decode("UTF-8") , "code": 200}), 200
        else:
            return jsonify({"data":"Login Was Invalid", "code": 403}), 403
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500


# jalankan program
if "__main__" == __name__:
	app.run(debug=True ,port=5001)