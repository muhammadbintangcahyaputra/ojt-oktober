<?php

// Statement dan Variable
// $nama = 'bulan';
// echo 'halo saya '.$nama;

// Operator Aritmatika
// $a = 5;
// $b = 3;

// $nilai = $a + $b;
// echo '<br>Penjumlahan : '.$nilai;
// $nilai = $a - $b;
// echo '<br>Pengurangan : '.$nilai;
// $nilai = $a * $b;
// echo '<br>Perkalian : '.$nilai;
// $nilai = $a / $b;
// echo '<br>Pembagian : '.$nilai;
// $nilai = $a % $b;
// echo '<br>Modulus : '.$nilai;

// Seleksi Kondisi
// $nilai = 79;
// if ($nilai >= 80) {
// 	echo "Anda dapat nilai : A";
// } else {
// 	echo "Anda tidak lulus";
// }

// Function 
function tampilkan($nama="bintang")
{
	echo 'haloo perkenalkan nama saya ' . $nama . '<br>';
}

tampilkan();
tampilkan('bulan');
tampilkan('bumi');

