# ambil library
from flask import Flask, jsonify

# inisiasi aplikasi flask
app = Flask(__name__)

# buat rute backend
@app.route("/")
def root():
	data = [
		{
			"nama" : "Buku",
			"harga" : 5000
		},
		{
			"nama" : "Tas",
			"harga" : 50000
		},
		{
			"nama" : "Pensil",
			"harga" : 4000
		},
	]
	return jsonify(data)


# jalankan program
if "__main__" == __name__:
	app.run(debug=True ,port=5001)